// Function: A_CrawlaCommanderReThink
//
// Description: Old Thinker for Crawla Commander.
//
// var1 = shoot bullets? -Wait, this was a thing?
// var2 = "pogo mode" speed
//
function A_CrawlaCommanderReThink(actor, var1, var2)

	local nextsector
	local thefloor

	if (actor.z >= actor.waterbottom and actor.watertop > actor.floorz
		and actor.z > actor.watertop - FixedMul(256*FRACUNIT, actor.scale))
		thefloor = actor.watertop
	else
		thefloor = actor.floorz
	end

	if (actor.fuse & 1)
		actor.flags2 = $|MF2_DONTDRAW
	else
		actor.flags2 = $1 & ~MF2_DONTDRAW
	end
	
	if (actor.reactiontime > 0)
		actor.reactiontime = $1 - 1
	end

	if (actor.fuse < 2)
		actor.fuse = 0
		actor.flags2 = $1 & ~MF2_FRET
	end

	// Hover mode
	if (actor.health > 1 or actor.fuse)
		if (actor.z < thefloor + FixedMul(16*FRACUNIT, actor.scale))
			actor.momz = $+ FixedMul(FRACUNIT, actor.scale)
		elseif (actor.z < thefloor + FixedMul(32*FRACUNIT, actor.scale))
			actor.momz = $+ FixedMul(FRACUNIT/2, actor.scale)
		else
			actor.momz = $+ FixedMul(16, actor.scale)
		end
	end

	if (not actor.target or not (actor.target.flags & MF_SHOOTABLE))
		// look for a new target
		if (P_LookForPlayers(actor, 0, true, false))
			return // got a new target
		end

		if (actor.state ~= actor.info.spawnstate) 
			actor.state = actor.info.spawnstate
		end
		return
	end

	local dist = R_PointToDist2(actor.x,  actor.y, actor.target.x, actor.target.y)

	if (actor.target.player and actor.health > 1)
		if (dist < FixedMul(128*FRACUNIT, actor.scale)
			and ((actor.target.player.pflags & PF_JUMPED) or (actor.target.player.pflags & PF_SPINNING)))
			// Auughnot  He's trying to kill younot  Strafenot  STRAAAAFFEEEnot not 
			if (actor.target.momx or actor.target.momy)
				P_InstaThrust(actor, actor.angle - ANGLE_180, FixedMul(20*FRACUNIT, actor.scale))
			end
			return
		end
	end

	if (var1)
		if (actor.health < 2 and P_RandomChance(FRACUNIT/128))
			P_SpawnMissile(actor, actor.target, var1)
		end
	end

	// Face the player
	actor.angle = R_PointToAngle2(actor.x, actor.y, actor.target.x, actor.target.y)

	if (actor.threshold and dist > FixedMul(256*FRACUNIT, actor.scale))
		actor.momx = 0
		actor.momy = 0
	end

	if (actor.reactiontime and actor.reactiontime <= 2*TICRATE and dist > actor.target.radius - FixedMul(FRACUNIT, actor.scale))
		actor.threshold = 0

		// Roam around, somewhat in the player's direction.
		actor.angle = $+ (P_RandomByte()<<10)
		actor.angle = $- (P_RandomByte()<<10)

		if (actor.health > 1)
			P_InstaThrust(actor, actor.angle, FixedMul(10*FRACUNIT, actor.scale))
		end

	elseif (not actor.reactiontime)

		if (actor.health > 1) // Hover Mode

			if (dist < FixedMul(512*FRACUNIT, actor.scale))

				actor.angle = R_PointToAngle2(actor.x, actor.y, actor.target.x, actor.target.y)
				P_InstaThrust(actor, actor.angle, FixedMul(60*FRACUNIT, actor.scale))
				actor.threshold = 1
			end
		end
		actor.reactiontime = 2*TICRATE + P_RandomByte()/2
	end

	if (actor.health == 1)
		P_Thrust(actor, actor.angle, 1)
	end

	// Pogo Mode
	if (not actor.fuse and actor.health == 1 and actor.z <= actor.floorz)

		if (dist < FixedMul(256*FRACUNIT, actor.scale))

			actor.momz = FixedMul(var2, actor.scale)
			actor.angle = R_PointToAngle2(actor.x, actor.y, actor.target.x, actor.target.y)
			P_InstaThrust(actor, actor.angle, FixedMul(var2/8, actor.scale))
			// pogo on player

		else

			local prandom = P_RandomByte()
			actor.angle = R_PointToAngle2(actor.x, actor.y, actor.target.x, actor.target.y)
			if P_RandomChance(FRACUNIT/2) then
				actor.angle = $ - prandom
			else
				actor.angle = $ + prandom
			end
			P_InstaThrust(actor, actor.angle, FixedDiv(FixedMul(var2, actor.scale), 3*FRACUNIT/2))
			actor.momz = FixedMul(var2, actor.scale) // Bounce up in air
		end
	end

	nextsector = R_PointInSubsector(actor.x + actor.momx, actor.y + actor.momy).sector

	// Move downwards or upwards to go through a passageway.
	if (nextsector.floorheight > actor.z and nextsector.floorheight - actor.z < FixedMul(128*FRACUNIT, actor.scale))
		actor.momz = $+ (nextsector.floorheight - actor.z) / 4
	end
end
