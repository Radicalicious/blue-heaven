freeslot("MT_RETROJETBULLET", "S_RJETBULLET1", "S_RJETBULLET2", "SPR_RJBU")

// Retro Jetty-Syn Gunner bullet
mobjinfo[MT_RETROJETBULLET] = {
        doomednum = -1,
        spawnstate = S_RJETBULLET1,
        spawnhealth = 1,
        seestate = S_NULL,
        seesound = sfx_None,
        reactiontime = 32,
        attacksound = sfx_None,
        painstate = S_NULL,
        painchance = 200,
        painsound = sfx_None,
		missilestate = S_NULL,
        deathstate = S_XPLD1,
        xdeathstate = S_NULL,
        deathsound = sfx_None,
        speed = 20*FRACUNIT,
        radius = 4*FRACUNIT,
        height = 8*FRACUNIT,
        dispoffset = 0,
        mass = 100,
        damage = 1,
        activesound = sfx_None,
		raisestate = S_NULL,
        flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY
}
	
states[S_RJETBULLET1] = {SPR_RJBU, FF_FULLBRIGHT|A, 4, nil, 0, 0, S_RJETBULLET2}
states[S_RJETBULLET2] = {SPR_RJBU, FF_FULLBRIGHT|B, 4, nil, 0, 0, S_RJETBULLET1}
