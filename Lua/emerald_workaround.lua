//
// Emerald workaround script for Blue Heaven. Remove this when Special Stage 6 and 7 are created!
//

addHook("PlayerThink", function(p)
	if (mapheaderinfo[gamemap].lvlttl != "Special Stage 5") then return end
	if ((p.exiting) and (emeralds == EMERALD1|EMERALD2|EMERALD3|EMERALD4|EMERALD5))
		emeralds = 127
	end
end)