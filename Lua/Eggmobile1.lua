freeslot("MT_EGGOMATIC", "S_EGGOMATIC_HOVER", "S_EGGOMATIC_HOVER1", "S_EGGOMATIC_HOVER2", "S_EGGOMATIC_LAZER", "S_EGGOMATIC_LAZER1", "S_EGGOMATIC_BEAM", "S_EGGOMATIC_BEAM1", "S_EGGOMATIC_RETREAT", "S_EGGOMATIC_RETREAT1", "S_EGGOMATIC_HIT", "S_EGGOMATIC_KABOOM", "S_EGGOMATIC_KABOOM1", "S_EGGOMATIC_KABOOM2", "S_EGGOMATIC_KABOOM3", "S_EGGOMATIC_KABOOM4", "S_EGGOMATIC_KABOOM5", "S_EGGOMATIC_KABOOM6", "S_EGGOMATIC_KABOOM7", "S_EGGOMATIC_KABOOM8", "S_EGGOMATIC_KABOOM9", "S_EGGOMATIC_KABOOM10", "S_EGGOMATIC_KABOOM11", "S_EGGOMATIC_KABOOM12", "S_EGGOMATIC_KABOOM13", "S_EGGOMATIC_RUNAWAY", "S_EGGOMATIC_RUNAWAY1", "S_EGGOMATIC_BEAM_HAULT", "S_EGGOMATIC_LAZER_HAULT", "SPR_EGG1")

//Special Thanks to Monster Iestyn, Flare, Flame, and anyone else on #scripting who helped make this possible.

local function Spk_SpawnSpikeShield(mobj)
    local spknum
    local spk
    
    if mobj and mobj.valid
        for spknum = 0, 3 do
            spk = P_SpawnMobj(mobj.x, mobj.y, mobj.z, MT_SPIKEBALL)
            spk.target = mobj
            spk.threshold = spknum+1 -- Tell these spikeballs apart
			--print("Spawning Spikeball #"+spk.threshold)
        end
    end
end

addHook("MobjSpawn", function(mo)
    mo.jetfume1 = P_SpawnMobj(mo.x, mo.y, mo.z, MT_JETFUMEC1)
    mo.jetfume2 = P_SpawnMobj(mo.x, mo.y, mo.z, MT_JETFUMEC1)
    mo.jetfume3 = P_SpawnMobj(mo.x, mo.y, mo.z, MT_JETFUMEC1)
    mo.jetfume1.target = mo
    mo.jetfume2.target = mo
    mo.jetfume3.target = mo
end, MT_EGGOMATIC)

addHook("MapThingSpawn", function(mobj, mapthing)
    if (mapthing.options & MTF_AMBUSH)
        Spk_SpawnSpikeShield(mo)
   end
end, MT_EGGOMATIC)

addHook("MobjThinker", function(mo)
    if mo.jetfume1 and mo.jetfume1.valid then
		P_MoveOrigin(mo.jetfume1, mo.x - 54 * cos(mo.angle), mo.y - 54 * sin(mo.angle), mo.z + (12*FRACUNIT))
    end
    if mo.jetfume2 and mo.jetfume2.valid then
		P_MoveOrigin(mo.jetfume2, mo.x - 54 * cos(mo.angle) - 22*cos(mo.angle-ANGLE_90), mo.y - 54 * sin(mo.angle) - 22*sin(mo.angle-ANGLE_90), mo.z + (40*FRACUNIT))
    end
    if mo.jetfume3 and mo.jetfume3.valid then
		P_MoveOrigin(mo.jetfume3, mo.x - 54 * cos(mo.angle) - 22*cos(mo.angle+ANGLE_90), mo.y - 54 * sin(mo.angle) - 22*sin(mo.angle+ANGLE_90), mo.z + (40*FRACUNIT))
    end
end, MT_EGGOMATIC)

addHook("MobjThinker", function(mo)
    -- Hacking in A_RotateSpikeBall a bit differently...
    local ns = 12*mo.info.speed
    local fa = (mo.threshold-1)*ANGLE_90 + ((leveltime%9)*ANG10)

    if mo.target and mo.target.valid
		if (mo.info.speed != 0) -- No Speed? Don't spin.
			P_MoveOrigin(mo,
						mo.target.x + FixedMul(cos(fa), ns),
						mo.target.y + FixedMul(sin(fa), ns),
						mo.target.z + mo.target.height/2)
		end
    end
end, MT_SPIKEBALL)