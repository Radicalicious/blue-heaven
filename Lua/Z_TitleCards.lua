--
-- Pre-v2.2 title card port by Radicalicious
--
-- Reusable (credit appreciated but not required)
--
-- requires amperbee's UDS library
--

FNT_NewFont("FONT_OLDCARD", {
	prefix = "OCFNT%03d",
	spacewidth = 16
})

FNT_NewFont("FONT_OLDCARDNUM", {
	prefix = "OTC%03d",
	spacewidth = 30
})

local BASEVIDWIDTH = 320
local BASEVIDHEIGHT = 200

addHook("PlayerSpawn", function(p)
	p.cardtics = leveltime
end)

hud.add(function(d, p)
	if not (mapheaderinfo[gamemap].oldtitlecard) then return end

	local lvlttl = string.upper(mapheaderinfo[gamemap].lvlttl)
	local subttl = mapheaderinfo[gamemap].subttl
	local actnum = tonumber(mapheaderinfo[gamemap].actnum)
	local lvlttlxpos
	local subttlxpos = BASEVIDWIDTH/2
	local ttlnumxpos
	local zonexpos

	local lvlttly
	local zoney
	
	local cardtics = leveltime-p.cardtics
	if (not cardtics) then cardtics = 0 end

	if (not (cardtics > 2 and cardtics-3 < 110))
		return
	end

	if (not actnum)
		actnum = 0
	end

	if (actnum > 0)
		lvlttlxpos = ((BASEVIDWIDTH/2) - (d.levelTitleWidth(string.lower(lvlttl))/2) - 16)
	else
		lvlttlxpos = ((BASEVIDWIDTH/2) - (d.levelTitleWidth(lvlttl)/2))
	end

	ttlnumxpos = lvlttlxpos + d.levelTitleWidth(string.lower(lvlttl))
	zonexpos = ttlnumxpos - (d.cachePatch("OCFNT090").width + d.cachePatch("OCFNT079").width + d.cachePatch("OCFNT078").width + d.cachePatch("OCFNT069").width)

	if (lvlttlxpos < 0)
		lvlttlxpos = 0
	end

	// There's no consistent algorithm that can accurately define the old positions
	// so I just ended up resorting to a single switct statement to define them
	
	local i = cardtics-3
	if (i == 0)
		zoney = 200
		lvlttly = 0
	elseif (i == 1)
		zoney = 188
		lvlttly = 12
	elseif (i == 2)
		zoney = 176
		lvlttly = 24
	elseif (i == 3)
		zoney = 164
		lvlttly = 36
	elseif (i == 4)
		zoney = 152
		lvlttly = 48
	elseif (i == 5)
		zoney = 140
		lvlttly = 60
	elseif (i == 6)
		zoney = 128
		lvlttly = 72
	elseif (i == 105)
		zoney = 80
		lvlttly = 104
	elseif (i == 106)
		zoney = 56
		lvlttly = 128
	elseif (i == 107)
		zoney = 32
		lvlttly = 152
	elseif (i == 108)
		zoney = 8
		lvlttly = 176
	elseif (i == 109)
		zoney = 0
		lvlttly = 200
	else
		zoney = 104
		lvlttly = 80
	end

	if (actnum > 0)
		FNT_Write(d, {
			x = ttlnumxpos*FRACUNIT,
			y = zoney*FRACUNIT,
			font = FONT_OLDCARDNUM,
			text = tostring(actnum)
		})
	end

	FNT_Write(d, {
		x = lvlttlxpos*FRACUNIT,
		y = lvlttly*FRACUNIT,
		font = FONT_OLDCARD,
		text = lvlttl
	})

	if (not (mapheaderinfo[gamemap].levelflags & LF_NOZONE))
		FNT_Write(d, {
			x = zonexpos*FRACUNIT,
			y = zoney*FRACUNIT,
			font = FONT_OLDCARD,
			text = "ZONE"
		})
	end

	if (lvlttly+48 < 200)
		d.drawString(subttlxpos, lvlttly+48, subttl, V_ALLOWLOWERCASE, "center")
	end
end, "game")