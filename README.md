# Blue Heaven

Blue Heaven is a level pack mod for Sonic Robo Blast 2 originally created between 2003 and 2007 by Bill Reed, known online as Tets. This GitLab page, managed by Radicalicious, serves the purpose of keeping the level pack up to date with current versions of SRB2 and fixing bugs from the original or caused by porting.

## How to use in SRB2

In v2.2.10 of SRB2 or higher, the use of this pack from Git is very easy: all you need to do is load the folder as an addon. Previous versions (v2.2.1 through v2.2.9) require you simply compress the folder as a ZIP file and rename the extension to ".pk3". If you don't want to mess with any of this, however, you should go download it off the SRB2 Message Board: (release link pending).